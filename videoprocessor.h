#ifndef VIDEOPROCESSOR_H
#define VIDEOPROCESSOR_H

#include <QObject>
#include <QPixmap>
#include <opencv4/opencv2/opencv.hpp>
#include "facialdetection.h"
class VideoProcessor : public QObject
{
    Q_OBJECT
public:
    explicit VideoProcessor(QObject *parent = nullptr);

signals:
    void outDisplay(QPixmap pixmap);

public slots:
    void startVideo();
    void stopVideo();
private:
    bool stopped;
    FacialDetection *facialdetection;
    size_t face_count{};
};

#endif // VIDEOPROCESSOR_H
