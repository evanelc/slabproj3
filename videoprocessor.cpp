#include "videoprocessor.h"


VideoProcessor::VideoProcessor(QObject *parent) : QObject(parent)
{

}

void VideoProcessor::startVideo() {
    cv::VideoCapture camera("../opencv-qt-starter/videos/Sample.mp4");
    cv::Mat inFrame, outFrame;
    stopped = false;
    camera.set(cv::CAP_PROP_FPS, 30);

    //instantiating a cascade and border
    cv::CascadeClassifier cascade = qrcCascade("../opencv-qt-starter/cascades/haarcascade_frontalface_alt.xml");
    QString border_path("../opencv-qt-starter/images/Admin.png");

    // create a new FacialDetection with the cascade and border_path we just created
    facialdetection = new FacialDetection(cascade, border_path);

    while(camera.isOpened() && !stopped) {
        camera >> inFrame;
        if(inFrame.empty())
            continue;


        cv::resize(inFrame, inFrame,cv::Size(640,480), 0, 0, cv::INTER_CUBIC);
//        outFrame = inFrame.clone();
        // Adjust outFrame here instead of clone

//        cv::Mat detectedFaces = facialdetection->detectAndDisplay(outFrame, &face_count);

        outFrame = facialdetection->detectAndDisplay(inFrame, &face_count);

        QImage faceImage = QImage(outFrame.data,
                                  outFrame.cols,
                                  outFrame.rows,
                                  outFrame.step,
                                  QImage::Format_RGB888)
                                    .rgbSwapped();
        emit outDisplay(QPixmap::fromImage(faceImage));
    }
}

void VideoProcessor::stopVideo() {
    stopped = true;
}
